# Keraleeyam - display style font for Malayalam #

It was in 2005 Keraleeyam font was designed for the environmental magazine
‘Keraleeyam’ under the editorship Robin published from Thrissur. Initially it
was designed for ASCII Rachana for typesetting the magazine using PageMaker. It
was visually modelled after the then popular bold ASCII fonts Leela and
Indulekha and Its geometry follows a vertical oval shape. Keraleeyam was made
Unicode in 2015.

Keraleeyam is a thick san-serif display style font and is
condensed. It is widely used for designing book covers and titles.

Conjucts, especially vertical conjuncts are reworked for better balance in
between above and below characters.

## License ##
RIT Keraleeyam is licensed under Open Font License 1.1

## Authors ##
Hussain KH (design, typography), Rajeesh KV (font engineering),
Rachana Institute of Typography (http://rachana.org.in)

## Colophon ##
This font is an offspring of Rachana movement under the leadership of 
R. Chitrajakumar who founded Rachana Akshara Vedi in 1999. He devised the
‘definitive character set’ of Malayalam based on traditional script which is
the base of Rachana as well as this font.

The naming convention of glyphs (`k1` for `ക`, `k2` for `ഖ` etc.) is devised
by K.H. Hussain in 2005 to form a mnemonic way to represent conjuncts and its
components and to facilitate the coding of glyph substitution of conjuncts.
