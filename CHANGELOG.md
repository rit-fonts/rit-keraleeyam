Version 3.2.1
=============
- Fix shaping of യ്തു/y1th1u1 in InDesign

Version 3.2
===========
- Support old shapers from Windows XP, old Pango, Lipika (Adobe) to HarfBuzz, Uniscribe
- Makefile: update tests
- Font build tool: reuse definitive shaping rules, using FontForge API to merge features

Version 3.1
===========
- [FIX #2] Rename vaiants to Bold & BoldItalic and fix the OS/2 StyleMap.

Version 3.0.1
=============
- [MAJOR] Rewritten Malayalam shaping rules: form post-base u1/u2 forms of Ra, below-base La first and then the rest of the conjuncts. This fixes a large chunk of issues with limited character set fonts - they now form Ra forms consistently
- Makefile: updated build directory & release targets, add TeX test file
- Add changelog
- GitLab automated release management

Version 3.0.0
=============
- Version 3.0. Adapted feature file names for Regular and Italic
- Add RIT Keraleeyam Italic source
- RIT-Keraleeyam → RIT-Keraleeyam-Regular
- Keraleeyam Regular: reduce width of 'space' character from 560 to 261
- [FIX #1] Add glyph for minus, copied from hyphen
- Add README for Keraleeyam
- Add GitLab CI/CD
- RIT Keraleeyam: initial version
